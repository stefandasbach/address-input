var geocoder;
$(document).ready(function(){$("#insert").validate({
	errorClass:'errors',
	rules: {
		address1: "required",
		city: "required",
		state: "required",
		zip: {
			required: true,
			minlength: 5
		}
	},
	messages: {
		address1: "Address required",
		city: "City required",
		state: "Please select a state",
		zip: { 
			required: "Zip code required",
			minlength: "5 digits required"
		}
	}
});
});
geocoder = new google.maps.Geocoder();

function getCoords(address1, address2, address3, city, state, zip) {
	var address = address1 +" "+ address2 +" "+ address3 +" "+ 
			city +" "+ state +" "+ zip;
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			var lat = results[0].geometry.location.lat();
			var lng = results[0].geometry.location.lng();
			$("#lat").val(lat);
			$("#lng").val(lng);
			alert($("#lat").val());
			alert($("#lng").val());
		}
		else {
			alert('Geocode was unsuccessful because: ' + status);
		}
	});
	return false;
}
